#!/bin/bash

readonly PROGNAME=$(basename $0)
readonly PROGDIR=$(readlink -m $(dirname $0))
readonly ARGS="$@"
readonly GRAPHITE_KEY="2f88b518-24dc-42de-b3a2-a84a9d094f54"
readonly GRAPHITE_PATH="iops."$(hostname -s)
readonly GRAPHITE_HOST="ef3a2323.carbon.hostedgraphite.com"
readonly GRAPHITE_REMOTE_PORT="2003"


myerror() {
    echo " [!] $1"
    exit 1
}
mywarn() {
    echo " [!] $1"
}
myinfo() {
    echo " [+] $1"
}

usage() {
    cat << EOF
$PROGNAME usage
EOF
}

cmdline() {
    while getopts "d:i:n:h" OPTION
    do
         case $OPTION in
         d)
             readonly MY_DEVICE=$OPTARG
             ;;
         i)
             readonly INTERVAL=$OPTARG
             ;;
         n)
             SAMPLES_NUMBER=$OPTARG
             ;;

         h)
             usage
             exit 0
             ;;
         *)
             usage
             exit 1
             ;;
        esac
    done

    if [ -z "$INTERVAL" ]; then
        myerror "Interval must be specified";
    fi

    readonly DEVICE="/dev/$MY_DEVICE"
    if [ ! -b "$DEVICE" ]; then
        myerror "Device is invalid";
    fi

    if [ -z "$SAMPLES_NUMBER" ]; then
        SAMPLES_NUMBER=0
    fi

}

get_sample_iostat() {
    IOSTAT_OUTPUT=$(iostat -x -t $INTERVAL 2 -d "$DEVICE" | grep "$MY_DEVICE " | tail -n 1 )
    READ_REQ=$(echo $IOSTAT_OUTPUT | awk '{print $2}');
    WRITE_REQ=$(echo $IOSTAT_OUTPUT | awk '{print $3}');
    TOTAL_REQ=$(awk "BEGIN {print $READ_REQ+$WRITE_REQ; exit}")
}


send_to_graphite () {
    local METRIC=$1
    local VALUE=$2
    myinfo "Sending to $GRAPHITE_KEY.$GRAPHITE_PATH.$METRIC : $VALUE"
    #echo "$GRAPHITE_KEY.$GRAPHITE_PATH.$METRIC $VALUE" | nc -uw0 $GRAPHITE_HOST $GRAPHITE_REMOTE_PORT #netcat fails with -w0
    echo "$GRAPHITE_KEY.$GRAPHITE_PATH.$METRIC $VALUE" | socat -t0 - UDP:$GRAPHITE_HOST:$GRAPHITE_REMOTE_PORT
}

main() {
    cmdline $ARGS
    myinfo "Sampling $DEVICE at $INTERVAL secs"

    i="0"
    while true; do
        let "i++"
        get_sample_iostat
        send_to_graphite "WRITE_REQ" $WRITE_REQ
        send_to_graphite "READ_REQ" $READ_REQ
        send_to_graphite "TOTAL_REQ" $TOTAL_REQ
        if [ "$SAMPLES_NUMBER" == "0" ]; then
            true
        else
            if [ "$i" -ge "$SAMPLES_NUMBER" ]; then
                break
            fi
        fi
    done

}
main